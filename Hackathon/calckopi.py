from abc import ABCMeta, abstractmethod


###############################################################################
# Pilih Kopi
###############################################################################
class Beverage:
    __metaclass__ = ABCMeta

    def __init__(self):
        self._description = "Unknown Beverage"

    def get_description(self):
        return self._description

    @abstractmethod
    def cost(self):
        pass


class arabica(Beverage):

    def __init__(self):
        self._description = "Arabica"

    def cost(self):
        return 24000


class robusta(Beverage):

    def __init__(self):
        self._description = "Robusta"

    def cost(self):
        return 27000


class Cappucino(Beverage):

    def __init__(self):
        self._description = "Cappucino"

    def cost(self):
        return 30000


class kopiJawa(Beverage):

    def __init__(self):
        self._description = "Kopi Jawa"

    def cost(self):
        return 15000


###############################################################################
# Condiment decorators (Size dan Topping)
###############################################################################

class CondimentDecorator(Beverage):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_description(self):
        pass


class Keju(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + ", Keju"

    def cost(self):
        return 5000 + self._beverage.cost()


class Float(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + ", Float"

    def cost(self):
        return 4000 + self._beverage.cost()


class Frape(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + ", Frape"

    def cost(self):
        return 6000 + self._beverage.cost()


class Coklat(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + ", Coklat"

    def cost(self):
        return 5000 + self._beverage.cost()

class Jumbo(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + " Jumbo"

    def cost(self):
        return 5000 + self._beverage.cost()

class Medium(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + " Medium"

    def cost(self):
        return 3000 + self._beverage.cost()

class Small(CondimentDecorator):

    def __init__(self, beverage):
        self._beverage = beverage

    def get_description(self):
        return self._beverage.get_description() + " Small"

    def cost(self):
        return self._beverage.cost()


###############################################################################
# Simulation
###############################################################################

if __name__ == '__main__':
    beverage = kopiJawa()
    print(beverage.get_description() + " $" + str(beverage.cost()))

    beverage2 = arabica()
    beverage2 = Coklat(beverage2)
    beverage2 = Float(beverage2)
    beverage2 = Frape(beverage2)
    print(beverage2.get_description() + " $" + str(beverage2.cost()))
