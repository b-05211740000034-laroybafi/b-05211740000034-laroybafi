from PyQt5 import QtCore, QtGui, QtWidgets
from ui_coffeeToffee import Ui_CoffeeToffee
from calckopi import *


class CoffeeToffeeWindow(QtWidgets.QMainWindow, Ui_CoffeeToffee, Beverage):

    firstnum = None
    harga = 0

    def __init__(self, parent=None):
        super(CoffeeToffeeWindow, self).__init__(parent=parent)
        self.setupUi(self)


        # PILIH KOPI
        self.radioButton.setChecked(True)

        # PILIH SIZE
        self.radioButton_5.setChecked(True)

        # TAMBAHAN

        # Proses
        self.pushButton.clicked.connect(self.proses)

        #button
        self.pushButton_3.clicked.connect(self.digit_pressed)
        self.pushButton_5.clicked.connect(self.digit_pressed)
        self.pushButton_6.clicked.connect(self.digit_pressed)
        self.pushButton_7.clicked.connect(self.digit_pressed)
        self.pushButton_8.clicked.connect(self.digit_pressed)
        self.pushButton_9.clicked.connect(self.digit_pressed)
        self.pushButton_10.clicked.connect(self.digit_pressed)
        self.pushButton_11.clicked.connect(self.digit_pressed)
        self.pushButton_12.clicked.connect(self.digit_pressed)
        self.pushButton_22.clicked.connect(self.digit_pressed)
        self.pushButton_23.clicked.connect(self.equal)
        self.pushButton_24.clicked.connect(self.operasi)
        self.pushButton_25.clicked.connect(self.operasi)
        self.pushButton_26.clicked.connect(self.operasi)
        self.pushButton_27.clicked.connect(self.operasi)
        self.pushButton_43.clicked.connect(self.digit_pressed)
        self.pushButton_44.clicked.connect(self.digit_pressed)
        self.pushButton_45.clicked.connect(self.digit_pressed)
        self.pushButton_46.clicked.connect(self.decimal_pressed)

        self.pushButton_24.setCheckable(True)
        self.pushButton_25.setCheckable(True)
        self.pushButton_26.setCheckable(True)
        self.pushButton_27.setCheckable(True)

    def digit_pressed(self):
        button = self.sender()

        if (self.pushButton_23.isChecked() or self.pushButton_24.isChecked() or self.pushButton_25.isChecked()
            or self.pushButton_23.isChecked()):
            newLabel = format(float(button.text()), '.15g')
        else:
            newLabel = format(float(self.textBrowser_2.toPlainText() + button.text()), '.15g')
        self.textBrowser_2.setText(newLabel)

    def decimal_pressed(self):
        self.textBrowser_2.setText(self.textBrowser_2.toPlainText() + '.')

    def operasi(self):
        button = self.sender()
        self.firstnum = float(self.textBrowser_2.toPlainText())
        button.setChecked(True)

    def equal(self):
        secondnumb = float(self.textBrowser_2.toPlainText())

        if self.pushButton_24.setCheckable(True):
            labelNumb = self.firstnum + secondnumb
            newLabel = format((labelNumb, '.15g'))
            self.textBrowser_2.setText(newLabel)
            self.pushButton_24.setCheckable(False)
        elif self.pushButton_25.setCheckable(True):
            labelNumb = self.firstnum - secondnumb
            newLabel = format((labelNumb, '.15g'))
            self.textBrowser_2.setText(newLabel)
            self.pushButton_25.setCheckable(False)
        elif self.pushButton_26.setCheckable(True):
            labelNumb = self.firstnum * secondnumb
            newLabel = format((labelNumb, '.15g'))
            self.textBrowser_2.setText(newLabel)
            self.pushButton_26.setCheckable(False)
        elif self.pushButton_27.setCheckable(True):
            labelNumb = self.firstnum / secondnumb
            newLabel = format((labelNumb, '.15g'))
            self.textBrowser_2.setText(newLabel)
            self.pushButton_27.setCheckable(False)

    def proses(self):
        beverage = None
        if self.radioButton.isChecked():
            beverage = arabica()
        elif self.radioButton_2.isChecked():
            beverage = robusta()
        elif self.radioButton_3.isChecked():
            beverage = cappucino()
        elif self.radioButton_4.isChecked():
            beverage = kopiJawa()

        if self.checkBox.isChecked():
            beverage = Keju(beverage)
        if self.checkBox_2.isChecked():
            beverage = Frape(beverage)
        if self.checkBox_3.isChecked():
            beverage = Float(beverage)
        if self.checkBox_4.isChecked():
            beverage = Coklat(beverage)

        if self.radioButton_5.isChecked():
            beverage = Jumbo(beverage)
        elif self.radioButton_6.isChecked():
            beverage = Medium(beverage)
        elif self.radioButton_7.isChecked():
            beverage = Small(beverage)

        hargaTotal = beverage.cost()
        desc = beverage.get_description()
        newLabel = format(hargaTotal)

        self.textBrowser.setText(
                                "Item : " + desc + "\n" +
                                "Total : Rp" + newLabel
                                )
